console.warn("Hello, 176!");

// Mini Activity:
/*
	How do we display tasks in the console.

	drink html
	eat javascript
	inhale css
	bake bootstrap

	Send a screenshot in our batch hangouts.
*/

console.log("drink html");
console.log("eat javascript");
console.log("inhale css");
console.log("bake bootstrap");


// Another way
let task1 = "drink html";
let task2 = "eat javascript";
let task3 = "inhale css";
let task4 = "bake bootstrap";

console.log(task1);
console.log(task2);
console.log(task3);
console.log(task4);

// Another way
let myTasks = ["drink html", "eat javascript", "inhale css", "bake bootstrap"];
console.log(myTasks);

/*
	Arrays are used to store multiple related values in a single variable.
	They are declared using square bracket[] also known as "Array Literals".

	Syntax:
		let/const arrayName = [elementA, elementB, elementC ... ];
*/

// Common Examples of Arrays
let grades = [98.5, 94.3, 89.2, 98.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];
let mixedArr = [12, `Asus`, null, undefined, {}, ``];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Reassigning array values
console.log(myTasks)
// arrayName[index]
myTasks[0] = "hellow world";
console.log(myTasks);

console.log(grades[1]);				// 94.3
console.log(computerBrands[6]);		// Toshiba
console.log(computerBrands[10]);	// undefined

// How do we get the number of elements in our array?
console.log(computerBrands.length); // 8
console.log(mixedArr.length); 		// 6

if (computerBrands.length > 5) {
	console.log("Too many Suppliers");
}

// How do we access the last element of our array?
let lastElementIndex = computerBrands.length - 1;
console.log(computerBrands[lastElementIndex]);

// Array Methods
// Mutator Methods
/*
	push()
		- Adds an element in the end of our array and returns the array's length

		Syntax:
			arrayName.push();
*/

let fruits = ["Apple", "Orange", "Kiwi", "Grapes","Dragon Fruit"];
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength); 			// 6
console.log(fruits);

// Adding multiple elements in our array
fruits.push("Avocado", "Guava");
console.log(fruits);

/*
	pop()
		- removes the last element in our array and returns the removed element

		Syntax:
			arrayName.pop();
*/

let removeFruit = fruits.pop();
console.log(removeFruit); 			// Guava
console.log(fruits);

/*
	unshift()
		- adds one or more elements at the beginning of an array

		Syntax:
			arrayName.unshift("elementA");
			arrayName.unshift("elementA", "elementB");
*/

fruits.unshift("Lime", "Banana");
console.log(fruits);

/*
	shift()
		- removes an element at the beginning of an array

		Syntax:
			arrayName.shift();
*/

fruits.shift();
console.log(fruits);

let anotherFruit = fruits.shift();
console.log(anotherFruit);				// Banana

/*
	splice()
		- simultaneously removes an element from a specified index number and add an element.

		Syntax:
			arrayName.splice(startingIndex, deleteCount);
			arrayName.splice(startingIndex, deleteCount, elementToBeAdded);
*/
console.log(fruits);
fruits.splice(1, 2, "Cherry", "Strawberry", "Durian", "Lansones");
//fruits.splice("start", 2, "Cherry", "Strawberry", "Durian", "Lansones");
console.log(fruits);

/*
	sort()
		- rearranges the array elements in alphanumreric order

		Syntax:
			arrayName.sort();
*/

fruits.sort();
console.log(fruits);

let arrayWithNumber = [5, 2, "Cherry", "Strawberry", "Durian", "1"];
arrayWithNumber.sort();
console.log(arrayWithNumber);

/*
	reverse()
		- reverses the order of array elements

		Syntax:
			arrayName.reverse();
*/

fruits.reverse();
console.log(fruits);

/*
	indexOf()
		- returns the index number of the first matching element found in an array

		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, fromIndex);
*/

let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "KR"];

let firstIndex = countries.indexOf("PH");
console.log(firstIndex);				// 1

firstIndex = countries.indexOf("PH", 3);
console.log(firstIndex);				// 5

let invalidCountry = countries.indexOf("RU");
console.log(invalidCountry);			// -1

/*
	lastIndexOf()
		- return the index number of the last matching element found in an array
		- the search will be done from the last element to the first element

		Syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);					// 5

lastIndex = countries.lastIndexOf("PH", 3);
console.log(lastIndex);					// 1

/*
	slice()
		- portions/slices elements from an array and returns a new array

		Syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
*/

console.log(countries);
let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log(slicedArrayB);

/*
	toString()
		- returns an array as a string separated by commas

		Syntax:
			arrayName.toString();
*/

let stringArray = countries.toString();
console.log(stringArray);

/*
	concat()
		- combines two arrays and returns the combined results

		Syntax:
			arrayA.concat(arrayB);
			arrayA.concat(arrayB, arrayC, ...)
*/

let taskArrayA = ["eat javascript", "drink html"];
let taskArrayB = ["inhale css", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let task = taskArrayA.concat(taskArrayB);
console.log(task);

let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTask);

// Combining arrays with elements
let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log(combinedTasks);

/*
	join()
		- Returns an array as a string separated by specified separator string

		Syntax:
			arrayName.join('separatorString');
*/

let users = ["John", "Jane", "Joe", "Sean"];

console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));

// Iteration Methods
/*
	forEach()
		- similar to a for loop that iterates on each array element

		Syntax:
			arrayName.forEach(function(indivElement) {
				statement
			});
*/

console.log(allTask);
allTask.forEach(function(task) {
	console.log(task);
})

// Using forEach with conditional statement

let filteredTasks = [];

allTask.forEach(function(task) {
	if (task.length > 10) {
		filteredTasks.push(task);
	}
});

console.log(filteredTasks);

/*
	map()
		- iterates on each element and returns a new array with a different value depending on the result of the function's operation

		Syntax:
			let/const resultArray = arrayName.map(function(indivElement) {
				statement;
			});
*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {
	return number * number;
});

console.log(numberMap);

/*
	every()
		- checks if all elements in an array met the given condition

		Syntax:
			let/const resultArray = arrayName.every(function(indivElement) {
				return (expression/condition);
			});
*/

let allValid = numbers.every(function(number) {
	return (number < 3);
});

console.log(allValid);					// false

/*
	some()
		- checks if at least one element met the given condition
		- returns true if at least one element met the given condition, and false if not

		Syntax:
			let/const resultArray.arrayName.some(function(indivElement) {
				return (expression/condition);
			});
*/

let someValid = numbers.some(function(number) {
	return (number < 2);
});

console.log(someValid);					// true

/*
	filter()
		- returns a new array that contains elements which met the given condition
		- it will return an empty array if none met the condition

		Syntax:
			let/const resultArray = arrayName.filter(function(indivElement) {
				return expression/condition;
			});
*/

let filterValid = numbers.filter(function(number) {
	return (number < 3);
});

console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
	return (number == 0);
});

console.log(nothingFound);

// Filtering using forEach()
let filteredNumber = [];

numbers.forEach(function(number) {
	if (number < 3) {
		filteredNumber.push(number);
	}
});

 console.log(filteredNumber);

 /*
	includes()
		-returns a boolean of true if it finds a matching item in an array
 */

 let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

 let filteredProducts = products.filter(function(product) {
 	return product.toLowerCase().includes('a') && product.toLowerCase().includes('p');
 });

 console.log(filteredProducts);

 // Multidimensional Arrays
/*
	Multidimentsional Arrays are useful for stroing complex data structures
*/

let chessBoard = [
	["a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9"],
	["b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9"],
	["c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9"],
	["d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9"],
	["e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9"],
	["f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9"],
	["g1", "g2", "g3", "g4", "g5", "g6", "g7", "g8", "g9"]
];

console.log(chessBoard);
// Accessing elements in multidimensional array

console.log(chessBoard[4][6]);			// e7
console.log(chessBoard[6][4]);			// g5
